package com.develop.DAO;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.develop.config.ConexionDB;
import com.develop.interfaces.ProductoI;
import com.develop.model.ProductoM;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ProductoDAO implements ProductoI{

	ConexionDB connections = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	Statement st = null;
	
	
	@Override
	public String registroProduto(ProductoM productom) {
		String sql ="insert into producto(nombre, fabricante, precio, existencias, fechaRegistroP) values(?,?,?,?,?)";
		
		
		try {
			consql = connections.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setString(1, productom.getNombre());
	        ps.setString(2, productom.getFabricante());
	        ps.setDouble(3, productom.getPrecio());
	        ps.setInt(4, productom.getExistencias());
	        ps.setTimestamp(5, Timestamp.valueOf(productom.getFechaRegistroProducto())); 
	         ps.execute();
	         return "OK";
		} catch (Exception e) {
			System.out.println("Error al guardar producto" + e);
		}
		return "";
	}


	@Override
	public List<ProductoM> listarProducto() {
		
		List<ProductoM> productosm = null;
		ProductoM prodm;
		String sql ="select id_producto, nombre, fabricante, precio, existencias, fechaRegistroP  from producto";
		
		try {
			consql = connections.getConnection();
			st = consql.createStatement();
			rs = st.executeQuery(sql);
			productosm = new ArrayList<>();
			while(rs.next() == true) {
				prodm = new ProductoM();
				prodm.setId_producto(rs.getInt("id_producto"));
				prodm.setNombre(rs.getString("nombre"));
				prodm.setFabricante(rs.getString("fabricante"));
				prodm.setPrecio(rs.getDouble("precio"));
				prodm.setExistencias(rs.getInt("existencias"));
				prodm.setFechaRegistroProducto(rs.getTimestamp("fechaRegistroP").toLocalDateTime());
				
				productosm.add(prodm);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return productosm;
	}


	@Override
	public boolean modificarProducto(int id_producto, String nombre, String fabricante, double precio, int existencias) {
		boolean filaActualizar=false;

		System.out.println(id_producto+" "+ nombre+" "+ fabricante+" "+ precio+" "+ existencias);
		String sql ="update producto set nombre=?, fabricante=?, precio=?, existencias=? where id_producto=?";
		try {
			consql = connections.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setInt(5, id_producto);
			ps.setString(1, nombre);
			ps.setString(2, fabricante);
			ps.setDouble(3, precio);
			ps.setInt(4, existencias);
			filaActualizar = ps.execute();
			System.out.println(filaActualizar);
		} catch (Exception e) {
			System.out.println(e);
		}
		return filaActualizar;
	}


	@Override
	public boolean eliminarProducto(ProductoM productom) {
		boolean filaEliminar=false;
		String sql ="delete from producto where id_producto=?";
		try {
			consql = connections.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setInt(1, productom.getId_producto());
			
			filaEliminar = ps.executeUpdate() > 0;
			System.out.println(filaEliminar + "Boleano de productoDAO");
		} catch (Exception e) {
			System.out.println(e);
		}
		return filaEliminar;
	}


	@Override
	public ProductoM seleccionarProducto(int id_producto) {
		String sql= "select nombre,fabricante,precio, existencias from producto where id_producto=?";
		ProductoM prodm;
		prodm = new ProductoM();
		try {
			consql = connections.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setInt(1, id_producto);
			rs = ps.executeQuery();
			while (rs.next()) {
				prodm.setNombre(rs.getString("nombre"));
				prodm.setFabricante(rs.getString("fabricante"));
				prodm.setPrecio(rs.getDouble("precio"));
				prodm.setExistencias(rs.getInt("existencias"));
			}
				} catch (SQLException e) {
			System.out.println(e);
		}
		return prodm;
	}

	
	
	@Override
	public String listadoProductos() {
		Document doc = new Document();
		String sql ="select * from producto";
		File carpeta = new File("C:/Users/Edson/Documents/Curso_java/CentroComercial/centrocomercial/Listados");
		if(!carpeta.exists()) {
			if (carpeta.mkdirs()) {
				System.out.println("Derectorio creado");
			}else {
				System.out.println("Error al crear directorio");
			}
		}
		try {
			consql = connections.getConnection();
			ps = consql.prepareStatement(sql);
			rs = ps.executeQuery();
			PdfWriter.getInstance(doc, new FileOutputStream(carpeta.getPath() + "/ListadoProductos.pdf"));
			doc.open();
			Paragraph titulo = new Paragraph();
			Paragraph parrafo = new Paragraph();
			titulo.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo.setAlignment(Paragraph.ALIGN_CENTER);
			titulo.add("Centro Comercial \n\n\n");
			parrafo.add("Listado de Productos Existentes");
			doc.add(titulo);
			doc.add(parrafo);
			PdfPTable tabla = new PdfPTable(6);
			PdfPCell celda1 = new PdfPCell(new Paragraph("Id_Producto"));
            PdfPCell celda2 = new PdfPCell(new Paragraph("Nombre"));
            PdfPCell celda3 = new PdfPCell(new Paragraph("Fabricante"));
            PdfPCell celda4 = new PdfPCell(new Paragraph("Precio"));
            PdfPCell celda5 = new PdfPCell(new Paragraph("Existencias"));
            PdfPCell celda6 = new PdfPCell(new Paragraph("Fecha de Registro"));
            tabla.addCell(celda1);
            tabla.addCell(celda2);
            tabla.addCell(celda3);
            tabla.addCell(celda4);
            tabla.addCell(celda5);
            tabla.addCell(celda6);
            while(rs.next()){
                tabla.addCell(rs.getInt(1)+"");
                tabla.addCell(rs.getString(2));
                tabla.addCell(rs.getString(3));
                tabla.addCell(rs.getDouble(4)+"");
                tabla.addCell(rs.getInt(5)+"");
                tabla.addCell(rs.getTimestamp(6).toLocalDateTime()+"");
                }
            doc.add(tabla);
            doc.close();
            return ("Exito");
		} catch (Exception e) {
			return ("Error en generar reporte");
		}
	}
	
	
	
}
