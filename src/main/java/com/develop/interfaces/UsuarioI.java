package com.develop.interfaces;

import com.develop.model.UsuarioM;

public interface UsuarioI {
	public String validaUsuario(String usuario, String password);
	
	public String registroUsuario(UsuarioM usuariom);
	
	public String listadoUsuarios();
	
}
