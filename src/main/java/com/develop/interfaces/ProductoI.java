package com.develop.interfaces;

import java.util.List;

import com.develop.model.ProductoM;

public interface ProductoI {

	public List<ProductoM> listarProducto();
	
	public String registroProduto(ProductoM productom);
	
	public boolean modificarProducto(int id_producto, String nombre, String fabricante, double precio, int existencias);
	
	public boolean eliminarProducto(ProductoM productom);
	
	public ProductoM seleccionarProducto(int id_prodcuto);
	
	public String listadoProductos();
	
	
}
