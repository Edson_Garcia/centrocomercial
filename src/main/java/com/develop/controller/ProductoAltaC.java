package com.develop.controller;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.model.ProductoM;

/**
 * Servlet implementation class ProductoAltaC
 */
@WebServlet("/ProductoAltaC")
public class ProductoAltaC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoAltaC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		ProductoM productom = new ProductoM();
		ProductoDAO dao= new ProductoDAO();
		
		productom.setNombre(request.getParameter("nombre"));
		productom.setFabricante(request.getParameter("fabricante"));
		productom.setPrecio(Double.parseDouble(request.getParameter("precio")));
		productom.setExistencias(Integer.parseInt(request.getParameter("existencias")));
		LocalDateTime fechaRegistroProducto = LocalDateTime.now();
		productom.setFechaRegistroProducto(fechaRegistroProducto);
		
		String respuesta = dao.registroProduto(productom);
		if (respuesta.equals("OK")) {
			System.out.println("Exito");
			request.getRequestDispatcher("/vistas/main/editarProducto.jsp").forward(request, response);
		}else {
			System.out.println("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
