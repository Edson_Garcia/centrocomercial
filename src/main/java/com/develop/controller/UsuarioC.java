package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class UsuarioC
 */
@WebServlet("/UsuarioC")
public class UsuarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String usuario;
		String password;
		
		usuario = request.getParameter("usuario");
		password = request.getParameter("password");
		UsuarioDAO dao = new UsuarioDAO();
		
		String respuesta = dao.validaUsuario(usuario, password);
		if (respuesta.equals("OK")) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/vistas/main/main.jsp");
			dispatcher.forward(request, response);
			System.out.println("Bienvenido al main");
		}else {
			System.out.println("No valido");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
