package com.develop.controller;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class UsuarioAltaC
 */
@WebServlet("/UsuarioAltaC")
public class UsuarioAltaC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioAltaC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		UsuarioM usuariom = new UsuarioM();
		UsuarioDAO dao = new UsuarioDAO();

		
		usuariom.setNombre(request.getParameter("nombre"));
		usuariom.setApellidop(request.getParameter("apellidop"));
		usuariom.setApellidom(request.getParameter("apellidom"));
		usuariom.setUsuario(request.getParameter("usuario"));
		usuariom.setCorreo(request.getParameter("correo"));
		usuariom.setPassword(request.getParameter("password"));
		
		String respuesta = dao.registroUsuario(usuariom);
		if (respuesta.equals("OK")) {
			System.out.println("Exito");
			request.getRequestDispatcher("/vistas/main/main.jsp").forward(request, response);
		}else {
			System.out.println("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
