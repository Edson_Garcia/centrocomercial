package com.develop.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.model.ProductoM;

/**
 * Servlet implementation class ProductoC
 */
@WebServlet("/ProductoC")
public class ProductoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		//String accion = request.getServletPath();
		String boton = request.getParameter("button");
		if(boton.equals("GuardarP")){
			ProductoM productom = new ProductoM();
			ProductoDAO dao= new ProductoDAO();
			
			productom.setNombre(request.getParameter("nombre"));
			productom.setFabricante(request.getParameter("fabricante"));
			productom.setPrecio(Double.parseDouble(request.getParameter("precio")));
			productom.setExistencias(Integer.parseInt(request.getParameter("existencias")));
			LocalDateTime fechaRegistroProducto = LocalDateTime.now();
			productom.setFechaRegistroProducto(fechaRegistroProducto);
			
			String respuesta = dao.registroProduto(productom);
			if (respuesta.equals("OK")) {
				System.out.println("Exito");
				request.getRequestDispatcher("/vistas/main/editarProducto.jsp").forward(request, response);
			}else {
				System.out.println("Error");
			}
		}
		
		if(boton.equals("EliminarP")){
			ProductoDAO dao= new ProductoDAO();
			ProductoM productom = new ProductoM();
			productom.setId_producto(Integer.parseInt(request.getParameter("id_producto")));
			dao.eliminarProducto(productom);
			//response.sendRedirect("list");
			
			boolean respuesta = dao.eliminarProducto(productom);
			if (respuesta == false) {
				System.out.println("Producto eliminado");
				request.getRequestDispatcher("/vistas/main/editarProducto.jsp").forward(request, response);
			}else {
				System.out.println("Error");
			}
		}
		
		if(boton.equals("ActualizarP")){
			ProductoDAO dao= new ProductoDAO();
			int id_producto = Integer.parseInt(request.getParameter("id_producto"));
			String nombre =request.getParameter("nombre");
			String fabricante = request.getParameter("fabricante");
			double precio = Double.parseDouble(request.getParameter("precio"));
			int existencias = Integer.parseInt(request.getParameter("existencias"));
			System.out.println(id_producto+" "+ nombre+" "+ fabricante+" "+ precio+" "+ existencias);
			boolean respuesta = dao.modificarProducto(id_producto, nombre, fabricante, precio, existencias);
			if (respuesta == false) {
				System.out.println("Producto Actualizado");
				request.getRequestDispatcher("/vistas/main/editarProducto.jsp").forward(request, response);
			}else {
				System.out.println("Error");
			}
		}
		
		if(boton.equals("Listar")){
			ProductoDAO dao= new ProductoDAO();
			
			String respuesta = dao.listadoProductos();
			if (respuesta.equals("Exito")) {
				System.out.println("Lista creada correctamente");
				//request.getRequestDispatcher("/vistas/main/editarProducto.jsp").forward(request, response);
			}else {
				System.out.println("Error");
			}
		}
		
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
