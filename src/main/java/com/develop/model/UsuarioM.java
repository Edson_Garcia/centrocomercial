package com.develop.model;

public class UsuarioM {
	private int id_uduario;
	private String nombre;
	private String apellidop;
	private String apellidom;
	private String usuario;
	private String correo;
	private String password;
	
	public UsuarioM() {
		super();
	}

	public UsuarioM(int id_uduario, String nombre, String apellidop, String apellidom, String usuario, String correo, String password) {
		super();
		this.id_uduario = id_uduario;
		this.nombre = nombre;
		this.apellidop = apellidop;
		this.apellidom = apellidom;
		this.usuario = usuario;
		this.correo = correo;
		this.password = password;
	}
	
	
	
	public int getId_uduario() {
		return id_uduario;
	}

	public void setId_uduario(int id_uduario) {
		this.id_uduario = id_uduario;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidop() {
		return apellidop;
	}

	public void setApellidop(String apellidop) {
		this.apellidop = apellidop;
	}
	
	public String getApellidom() {
		return apellidom;
	}
	
	public void setApellidom(String apellidom) {
		this.apellidom = apellidom;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
