package com.develop.model;

import java.sql.Date;
import java.time.LocalDateTime;

public class ProductoM {
	private int id_producto;
	private String nombre;
	private String fabricante;
	private double precio;
	private int existencias;
	private LocalDateTime fechaRegistroProducto;
	
	public ProductoM() {
		super();
	}

	public ProductoM(int id_producto, String nombre, String fabricante, double precio, int existencias, LocalDateTime fechaRegistroProducto) {
		super();
		this.id_producto = id_producto;
		this.nombre = nombre;
		this.fabricante = fabricante;
		this.precio = precio;
		this.existencias = existencias;
		this.fechaRegistroProducto = fechaRegistroProducto;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getExistencias() {
		return existencias;
	}

	public void setExistencias(int existencias) {
		this.existencias = existencias;
	}

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}

	public LocalDateTime getFechaRegistroProducto() {
		return fechaRegistroProducto;
	}

	public void setFechaRegistroProducto(LocalDateTime fechaRegistroProducto) {
		this.fechaRegistroProducto = fechaRegistroProducto;
	}

	

	
	
	

	
	
}
