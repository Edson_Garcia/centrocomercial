<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="/vistas/header.jsp"%>
<!DOCTYPE html>
<html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>

<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>
<div class="container">
        <div class="row">
			<div class="col-md-5 mx-auto">
			<div id="first">
				<div class="myform form ">
					 <div class="logo mb-3">
						 <div class="col-md-12 text-center">
							<h1>Registrar Productos</h1>
						 </div>
					</div>
	                   <form method="post" action="../../ProductoAltaC" >
						  <div class="form-group">
						    <label for="nameProduct">Nombre del Producto</label>
						    <input type="text" class="form-control" name="nombre"  placeholder="Nombre del Producto">
						  </div>
						  <div class="form-group">
						    <label for="nameProduct">Fabricante</label>
						    <input type="text" class="form-control" name="fabricante"  placeholder="Fabricante">
						  </div>
						  <div class="form-group">
						    <label for="nameProduct">Precio</label>
						    <input type="text" class="form-control" name="precio"  placeholder="Precio">
						  </div>
						  <div class="form-group">
						    <label for="nameProduct">Existencias</label>
						    <input type="text" class="form-control" name="existencias"  placeholder="Existencias">
						  </div>
						  <button type="submit" class="btn btn-primary">Guardar</button>
					  </form>
				</div>
			</div>
		 </div>
		</div>
      </div>   
</body>
<%@include file="/vistas/footer.jsp"%>
</html>