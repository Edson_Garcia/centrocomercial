-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2021 at 06:00 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `centrocomercial`
--

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `fabricante` varchar(25) NOT NULL,
  `precio` double NOT NULL,
  `existencias` int(11) NOT NULL,
  `fechaRegistroP` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `fabricante`, `precio`, `existencias`, `fechaRegistroP`) VALUES
(6, 'Maria', 'Games', 19, 4, '2021-10-15 15:02:48'),
(8, 'Papas', 'Sabritas', 13, 23, '2021-10-15 15:31:02'),
(9, 'Donitas', 'Bimbo', 15, 3, '2021-10-15 15:33:02'),
(10, 'Chocorol', 'Marinela', 9, 23, '2021-10-15 15:35:07'),
(11, 'Conchas', 'Biembo', 10, 23, '2021-10-15 15:49:13'),
(12, 'Mantecadas', 'Bimbo', 15, 34, '2021-10-15 15:49:31'),
(14, 'Panque Marmol', 'Bimbo', 35, 7, '2021-10-15 16:12:00'),
(15, 'Takis', 'Barcel', 8, 12, '2021-10-15 17:06:44'),
(16, 'Mancanita', 'Coca cola', 13, 7, '2021-10-15 17:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apellidop` varchar(30) NOT NULL,
  `apellidom` varchar(30) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `password` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellidop`, `apellidom`, `usuario`, `correo`, `password`) VALUES
(1, 'Edson', 'Garci', 'Rojas', 'edsono', 'edsoon93', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
